//! - UFO3 <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#woff-data>
//! - W3C <https://www.w3.org/TR/WOFF/>


/// WOFF Data
pub struct Woff {
    pub major_version: u8,
    pub minor_version: u8,
    pub metadata_unique_id: UniqueID,
    pub metadata_vendor: Vendor,
    pub metadata_credits: Credit,
    pub metadata_description: Description,
    pub metadata_licence: Licence,
    pub metadata_copyright: Copyright,
    pub metadata_trademark: Trademark,
    pub metadata_licensee: Licensee,
    pub metadata_extensions: Vec<Extension>
}

pub struct UniqueID {
    pub id: String,
}

pub struct Vendor {
    pub name: String,
    pub url: Option<String>,
    pub dir: Option<String>,
    pub class: Option<String>,
}

pub struct Credit {
    pub name: String,
    pub url: Option<String>,
    pub role: Option<String>,
    pub dir: Option<String>,
    pub class: Option<String>,
}

pub struct Description {
    pub url: Option<String>,
    pub text: Vec<Text>
}

pub struct Licence {
    pub url: Option<String>,
    pub id: Option<String>,
    pub text: Vec<Text>
}

pub struct Copyright {
    pub text: Vec<Text>
}

pub struct Trademark {

}

pub struct Licensee {

}

pub struct Text {

}

pub struct Extension {

}

pub struct ExtensioneItem {
    
}

pub struct ExtensionName {

}

pub struct ExtensionValue {

}

pub struct Guideline {

}
