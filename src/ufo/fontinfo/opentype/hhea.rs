//! - UFO3 <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#opentype-hhea-table-fields>
//! - OpenType <https://learn.microsoft.com/en-us/typography/opentype/spec/hhea>

/// OpenType `hhea` Table Fields
#[derive(Debug)]
pub struct Hhea {
    pub ascender: i32,
    pub descender: i32,
    pub line_gap: i32,
}
