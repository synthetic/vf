//! UFO3 <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#guidelines>

#[derive(Debug)]
pub struct Guideline {
    pub x: f32,
    pub y: f32,
    pub angle: f32,
    pub name: Option<String>,
    pub colour: Option<String>,
    pub identifier: Option<String>
}
