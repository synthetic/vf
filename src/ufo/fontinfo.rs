//! <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/>

pub mod guidelines;
pub mod opentype;
pub mod postscript;
pub mod woff;

use opentype::Opentype;
use postscript::Postscript;

#[derive(Debug)]
pub struct FontInfo {
    pub identification: Identification,
    pub legal: Legal,
    pub dimension: Dimension,
    pub misc: Option<Miscellaneous>,
    pub opentype: Opentype,
    pub postscript: Postscript,
}

/// Generic Identification Information
/// <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-identification-information>
#[derive(Debug)]
pub struct Identification {
    /// Family name. Note: The specification is agnostic about how this value relates to openTypeNamePreferredFamilyName.
    pub family_name: String,
    /// Style name. Note: The specification is agnostic about how this value relates to openTypeNamePreferredSubfamilyName.
    pub style_name: String,
    /// Family name used for bold, italic and bold italic style mapping.
    pub style_map_family_name: Option<String>,
    /// Style map style. The possible values are regular, italic, bold and bold italic. These are case sensitive.
    pub style_map_style_name: Option<String>,
    /// Major version.
    pub version_major: u8,
    /// Minor version.
    pub version_minor: u8,
    /// The year the font was created. This attribute is deprecated as of version 2. Its presence should not be relied upon by authoring tools. However, it may occur in a font’s info so authoring tools should preserve it if present.
    pub year: u16
}

/// Generic Legal Information
/// <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-legal-information>
#[derive(Debug)]
pub struct Legal {
    /// Copyright statement.
    pub copyright: String,
    /// Trademark statement.
    pub trademark: String
}

/// Generic Dimension Information
/// <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-dimension-information>
#[derive(Debug)]
pub struct Dimension {
    /// Units per em.
    pub units_per_em: f32,
    /// Descender value. Note: The specification is agnostic about the relationship to the more specific vertical metric values.
    pub descender: f32,
    /// x-height value.
    pub x_height: f32,
    /// Cap height value.
    pub cap_height: f32,
    /// Ascender value. Note: The specification is agnostic about the relationship to the more specific vertical metric values.
    pub ascender: f32,
    /// Italic angle. This must be an angle in counter-clockwise degrees from the vertical.
    pub italic_angle: f32,
}

/// Generic Miscellaneous Information
/// <https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-miscellaneous-information>
#[derive(Debug)]
pub struct Miscellaneous {
    /// Arbitrary note about the font.
    pub note: Option<String>
}
