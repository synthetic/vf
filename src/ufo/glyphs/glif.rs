//! <https://unifiedfontobject.org/versions/ufo3/glyphs/glif/>

use kfl::{Decode, DecodeScalar};
use kfl_plist::Dict;

#[derive(Clone, Debug, Decode)]
pub struct Glyph {
    #[kfl(property)]
    pub name: String,
    #[kfl(property)]
    pub format: String,
    #[kfl(child, default)]
    pub advance: Option<Advance>,
    #[kfl(child)]
    pub unicode: Option<Unicode>,
    // // image: Image,
    #[kfl(children)]
    pub guidelines: Vec<Guideline>,
    #[kfl(children)]
    pub anchors: Vec<Anchor>,
    #[kfl(child)]
    pub outline: Option<Outline>,
    // note: Option<String>,
    #[kfl(child)]
    pub lib: Option<Dict>
}

#[derive(Clone, Debug, Decode, Default)]
pub struct Advance {
    #[kfl(property, default)]
    pub width: Option<f32>,
    #[kfl(property, default)]
    pub height: Option<f32>
}

#[derive(Clone, Debug, Decode)]
pub struct Unicode {
    #[kfl(property)]
    pub hex: String
}

#[derive(Clone, Debug, Decode)]
pub struct Image {
    pub file_name: String
}

#[derive(Clone, Debug, Decode)]
pub struct Guideline {
    #[kfl(property)]
    pub x: f32,
    #[kfl(property)]
    pub y: f32,
    #[kfl(property)]
    pub angle: Option<f32>,
    #[kfl(property)]
    pub name: Option<String>,
    #[kfl(property)]
    pub colour: Option<String>,
    #[kfl(property)]
    pub identifier: Option<String>
}

#[derive(Clone, Debug, Decode)]
pub struct Anchor {
    #[kfl(property, default)]
    pub x: Option<f32>,
    #[kfl(property, default)]
    pub y: Option<f32>,
    #[kfl(property)]
    pub name: Option<String>,
    #[kfl(property)]
    pub colour: Option<String>,
    #[kfl(property)]
    pub identifier: Option<String>
}

#[derive(Clone, Debug, Decode)]
pub struct Outline {
    #[kfl(children)]
    pub components: Vec<Component>,
    #[kfl(children)]
    pub contours: Vec<Contour>
}

#[derive(Clone, Debug, Decode)]
pub struct Component {
    #[kfl(property)]
    pub base: Option<String>,
    #[kfl(property, default)]
    pub x_scale: Option<i32>,
    #[kfl(property, default)]
    pub y_scale: Option<i32>,
    #[kfl(property, default)]
    pub x_offset: Option<i32>,
    #[kfl(property, default)]
    pub y_offset: Option<i32>,
    #[kfl(property, default)]
    pub identifier: Option<String>
}

#[derive(Clone, Debug, Decode)]
pub struct Contour {
    #[kfl(property)]
    pub identifier: Option<String>,
    pub points: Vec<Point>
}

#[derive(Clone, Debug, Decode)]
pub struct Point {
    #[kfl(property, default)]
    pub x: Option<f32>,
    #[kfl(property, default)]
    pub y: Option<f32>,
    #[kfl(property, default)]
    pub r#type: PointType,
    #[kfl(property)]
    pub smooth: Option<bool>,
    #[kfl(property)]
    pub name: Option<String>,
    #[kfl(property)]
    pub identifier: Option<String>
}

#[derive(Clone, Debug, DecodeScalar, Default)]
pub enum PointType {
    Move,
    Line,
    #[default]
    Offcurve,
    Curve,
    Qcurve
}
